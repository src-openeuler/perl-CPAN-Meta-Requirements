%define mod_name CPAN-Meta-Requirements
Name:      perl-%{mod_name}
Version:   2.143
Release:   3
Summary:   A set of version requirements for a CPAN dist
License:   GPL-1.0-or-later OR Artistic-1.0-Perl
URL:       https://metacpan.org/release/%{mod_name}
Source0:   https://cpan.metacpan.org/authors/id/R/RJ/RJBS/%{mod_name}-%{version}.tar.gz
BuildArch: noarch

%define six_digit_version %(LC_ALL=C; printf '%.6f' '%{version}')
Provides:  perl(CPAN::Meta::Requirements) = %{six_digit_version}
BuildRequires:  perl-interpreter perl-generators perl(ExtUtils::MakeMaker) perl(Test::More) coreutils findutils make
BuildRequires:  perl(:VERSION) >= 5.10
Requires:  perl(B) perl(version) >= 0.88

%description
A CPAN::Meta::Requirements object models a set of version constraints like those specified in the META.yml 
or META.json files in CPAN distributions, and as defined by CPAN::Meta::Spec; It can be built up by adding 
more and more constraints, and it will reduce them to the simplest representation.
Logically impossible constraints will be identified immediately by thrown exceptions.

%package help
Summary:   Documents for %{name}
Buildarch: noarch

%description help
Man page and other related documents for %{name}.

%prep
%autosetup -n %{mod_name}-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1 UNINST=0
%make_build

%install
%make_install
%{_fixperms} -c %{buildroot}

%check
make test

%files
%license LICENSE
%doc Changes perlcritic.rc README
%{perl_vendorlib}/*

%files help
%doc CONTRIBUTING.mkdn
%{_mandir}/*/*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 2.143-3
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Oct 25 2023 huyubiao <huyubiao@huawei.com> - 2.143-2
- fix Source0 link error, fix Provides version error, add BuildRequires perl >= 5.10

* Wed Jul 12 2023 leeffo <liweiganga@uniontech.com> - 2.143-1
- upgrade to verison 2.143

* Tue Oct 25 2022 huyubiao <huyubiao@huawei.com> - 2.140-420
- define mod_name to opitomize the specfile

* Tue Sep 3 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.140-419 
- Package init
